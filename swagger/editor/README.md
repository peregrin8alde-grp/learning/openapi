# Swagger Editor

https://swagger.io/docs/open-source-tools/swagger-editor/

## docker

https://hub.docker.com/r/swaggerapi/swagger-editor/

```
docker run \
  -d \
  --name sw-editor \
  -p 10080:8080 \
  swaggerapi/swagger-editor
```

