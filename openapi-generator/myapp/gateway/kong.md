# Kong

https://konghq.com/

Docker でのインストール／セットアップ済み想定とする。

```
curl -i -X POST http://localhost:8001/services \
 --data name=python-flask-sv \
 --data url='http://python-flask-sv:8080'

curl -i -X POST http://localhost:8001/services/python-flask-sv/routes \
  --data 'paths[]=/python-flask-sv' \
  --data 'name=python-flask-sv'

curl -i -X GET http://localhost:8000/python-flask-sv/myapp/api/v1/todo
```

https://docs.konghq.com/hub/kong-inc/cors/

```
curl -X POST http://localhost:8001/services/python-flask-sv/plugins \
  --data "name=cors"  \
  --data "config.origins=*" \
  --data "config.methods=GET" \
  --data "config.methods=POST" \
  --data "config.methods=PUT" \
  --data "config.methods=PATCH" \
  --data "config.methods=DELETE"

curl -X POST http://localhost:8001/routes/python-flask-sv/plugins \
  --data "name=cors"  \
  --data "config.origins=*" \
  --data "config.methods=GET" \
  --data "config.methods=POST" \
  --data "config.methods=PUT" \
  --data "config.methods=PATCH" \
  --data "config.methods=DELETE"

curl http://localhost:8001/services/python-flask-sv/plugins
```
