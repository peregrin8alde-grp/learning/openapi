# Mongo DB

```
docker run \
  --name mongo \
  -d \
  -v mongodb:/data/db \
  --network kong-net \
  mongo
```

```
mongo

show dbs
use test_database

show collections

db.todo_collection.drop()
db.todo_collection.find()
```
