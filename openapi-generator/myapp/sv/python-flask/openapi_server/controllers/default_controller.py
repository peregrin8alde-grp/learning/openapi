import connexion
import six

from openapi_server.models.item import Item  # noqa: E501
from openapi_server import util

import logging

import pymongo
from pymongo import MongoClient
from pymongo.collection import ReturnDocument

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

client = MongoClient('mongodb://mongo:27017/')
db = client.test_database

def todo_get():  # noqa: E501
    """todo_get

     # noqa: E501


    :rtype: List[Item]
    """
    items = []

    todos = db.todo_collection
    find = todos.find(projection={'item': True})
    for db_item in find:
        logger.debug(db_item)
        item = db_item["item"]
        if connexion.request.is_json:
            item = Item.from_dict(item)
        
        logger.debug(item)
        items.append(item)

    return items


def todo_post(item):  # noqa: E501
    """todo_post

     # noqa: E501

    :param item: 
    :type item: dict | bytes

    :rtype: Item
    """
    if connexion.request.is_json:
        item = Item.from_dict(item)

    logger.debug(item)

    db_item = {
        "_comment": "ユーザに見せる以外にも情報あるはずなので、DB用の構造は別にする",
        "meta": "メタ情報など",
        "item": item.to_dict()
    }
    logger.debug(db_item)

    todos = db.todo_collection
    todo_id = todos.insert_one(db_item).inserted_id

    logger.debug(todo_id)

    return item


def todo_uid_delete(uid):  # noqa: E501
    """todo_uid_delete

     # noqa: E501

    :param uid: ID
    :type uid: str

    :rtype: None
    """
    todos = db.todo_collection
    result = todos.delete_one({"item.uid": uid})
    
    logger.debug(result.deleted_count)

    return { }


def todo_uid_get(uid):  # noqa: E501
    """todo_uid_get

     # noqa: E501

    :param uid: ID
    :type uid: str

    :rtype: Item
    """
    todos = db.todo_collection
    db_item = todos.find_one({"item.uid": uid})

    if db_item is None:
        item = {}
    else:
        logger.debug(db_item)
        item = db_item["item"]
    logger.debug(item)
        
    if connexion.request.is_json:
        item = Item.from_dict(item)
    logger.debug(item)

    return item


def todo_uid_patch(uid, item):  # noqa: E501
    """todo_uid_patch

     # noqa: E501

    :param uid: ID
    :type uid: str
    :param item: 
    :type item: dict | bytes

    :rtype: Item
    """
    if connexion.request.is_json:
        # 現状、無駄な処理だが、DB格納前のバリデーションや加工処理追加時には
        # クラスとして扱いため実施しておく
        item = Item.from_dict(item)
    
    item.uid = uid
    
    todos = db.todo_collection
    item_dict = item.to_dict()
    logger.debug(item_dict)

    updates = {}
    for key, value in item_dict.items():
        if value is not None:
            updates["item." + key] = value
    logger.debug(updates)

    result_db_item = todos.find_one_and_update(
        {"item.uid": uid}, 
        {'$set': updates}, 
        return_document=ReturnDocument.AFTER
    )
    
    logger.debug(result_db_item)

    if result_db_item is None:
        item = {}
    else:
        item = result_db_item["item"]
    
    logger.debug(item)
    if connexion.request.is_json:
        item = Item.from_dict(item)

    logger.debug(item)

    return item

def todo_uid_put(uid, item):  # noqa: E501
    """todo_uid_put

     # noqa: E501

    :param uid: ID
    :type uid: str
    :param item: 
    :type item: dict | bytes

    :rtype: Item
    """
    if connexion.request.is_json:
        item = Item.from_dict(item)
    
    item.uid = uid
    
    new_db_item = {
        "_comment": "ユーザに見せる以外にも情報あるはずなので、DB用の構造は別にする",
        "meta": "メタ情報など",
        "item": item.to_dict()
    }
    logger.debug(new_db_item)
    todos = db.todo_collection
    result_db_item = todos.find_one_and_replace({"item.uid": uid}, new_db_item, return_document=ReturnDocument.AFTER)
    
    logger.debug(result_db_item)

    if result_db_item is None:
        item = {}
    else:
        item = result_db_item["item"]
    
    logger.debug(item)
    if connexion.request.is_json:
        item = Item.from_dict(item)

    logger.debug(item)

    return item
