#!/usr/bin/env python3

import connexion
import logging

from openapi_server import encoder


def main():
    format = "%(levelname)s\tlocation:%(pathname)s(%(lineno)s)\t%(funcName)s\t%(message)s"
    logging.basicConfig(filename='myapp.log', level=logging.INFO, format=format)
 
    app = connexion.App(__name__, specification_dir='./openapi/')
    app.app.json_encoder = encoder.JSONEncoder
    app.add_api('openapi.yaml',
                arguments={'title': 'myapp'},
                pythonic_params=True)
    app.run(port=8080)


if __name__ == '__main__':
    main()
