# coding: utf-8

from __future__ import absolute_import
import unittest

from flask import json
from six import BytesIO

from openapi_server.models.item import Item  # noqa: E501
from openapi_server.test import BaseTestCase


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""

    def test_todo_get(self):
        """Test case for todo_get

        
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/myapp/api/v1/todo',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_todo_post(self):
        """Test case for todo_post

        
        """
        item = {
  "id" : 1,
  "task" : "todo it",
  "isDone" : false
}
        headers = { 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        response = self.client.open(
            '/myapp/api/v1/todo',
            method='POST',
            headers=headers,
            data=json.dumps(item),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_todo_uid_get(self):
        """Test case for todo_uid_get

        
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/myapp/api/v1/todo/{uid}'.format(uid='uid_example'),
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_todo_uid_put(self):
        """Test case for todo_uid_put

        
        """
        item = {
  "id" : 1,
  "task" : "todo it",
  "isDone" : false
}
        headers = { 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        response = self.client.open(
            '/myapp/api/v1/todo/{uid}'.format(uid='uid_example'),
            method='PUT',
            headers=headers,
            data=json.dumps(item),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    unittest.main()
