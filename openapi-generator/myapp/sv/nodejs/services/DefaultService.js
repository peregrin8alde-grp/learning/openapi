/* eslint-disable no-unused-vars */
const Service = require('./Service');

const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

// Connection URL
const url = 'mongodb://mongo:27017';

// Database Name
const dbName = 'myproject';

// Create a new MongoClient
const client = new MongoClient(url);


/**
*
* returns List
* */
const todoGET = () => new Promise(
  async (resolve, reject) => {
    try {

      // Use connect method to connect to the Server
      client.connect(function (err) {
        assert.equal(null, err);
        console.log("Connected successfully to server");

        const db = client.db(dbName);

        items = [];
        resolve(Service.successResponse(items));

        client.close();
      });
    } catch (e) {
      reject(Service.rejectResponse(
        e.message || 'Invalid input',
        e.status || 405,
      ));
    }
  },
);
/**
*
* item Item 
* returns Item
* */
const todoPOST = ({ item }) => new Promise(
  async (resolve, reject) => {
    try {
      resolve(Service.successResponse({
        item,
      }));
    } catch (e) {
      reject(Service.rejectResponse(
        e.message || 'Invalid input',
        e.status || 405,
      ));
    }
  },
);
/**
*
* uid String ID
* no response value expected for this operation
* */
const todoUidDELETE = ({ uid }) => new Promise(
  async (resolve, reject) => {
    try {
      resolve(Service.successResponse({
        uid,
      }));
    } catch (e) {
      reject(Service.rejectResponse(
        e.message || 'Invalid input',
        e.status || 405,
      ));
    }
  },
);
/**
*
* uid String ID
* returns Item
* */
const todoUidGET = ({ uid }) => new Promise(
  async (resolve, reject) => {
    try {
      resolve(Service.successResponse({
        uid,
      }));
    } catch (e) {
      reject(Service.rejectResponse(
        e.message || 'Invalid input',
        e.status || 405,
      ));
    }
  },
);
/**
*
* uid String ID
* item Item 
* returns Item
* */
const todoUidPATCH = ({ uid, item }) => new Promise(
  async (resolve, reject) => {
    try {
      resolve(Service.successResponse({
        uid,
        item,
      }));
    } catch (e) {
      reject(Service.rejectResponse(
        e.message || 'Invalid input',
        e.status || 405,
      ));
    }
  },
);
/**
*
* uid String ID
* item Item 
* returns Item
* */
const todoUidPUT = ({ uid, item }) => new Promise(
  async (resolve, reject) => {
    try {
      resolve(Service.successResponse({
        uid,
        item,
      }));
    } catch (e) {
      reject(Service.rejectResponse(
        e.message || 'Invalid input',
        e.status || 405,
      ));
    }
  },
);

module.exports = {
  todoGET,
  todoPOST,
  todoUidDELETE,
  todoUidGET,
  todoUidPATCH,
  todoUidPUT,
};
