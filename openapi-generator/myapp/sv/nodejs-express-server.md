# nodejs-express-server

https://github.com/OpenAPITools/openapi-generator/blob/master/docs/generators/nodejs-express-server.md

まだベータ版のため、バグが多そう。

```
java -jar ../../../../lib/openapi-generator-cli.jar \
    generate \
    -i ../doc/openapi.json \
    -g nodejs-express-server \
    -o ./nodejs \
    --ignore-file-override ./nodejs/openapi-generator-ignore
```

- (CORS)[https://developer.mozilla.org/ja/docs/Web/HTTP/CORS] はデフォルトで許可している模様（`this.app.use(cors())`）。
- レスポンスをいじるのに自動生成の API を編集する必要がありそう。できれば自動生成分は修正したくないため、別モジュールの利用も検討。
  - 自動生成時に上書きしない設定は可能だが、どれを対象にすべきで、どう自動生成分を反映するか要検討
    - `DefaultController.js` / `DefaultService.js` 以外は特に変化しない？
    - `DefaultService.js` の各メソッドを修正して実装する方法が考えられる。
- `requestBody` に対する名前の与え方が不明で自動で `item` となっている、かつ、`requestParams.body` というコードが生成されているせいで、
  `const todoIdPUT = ({ id, item }) => new Promise(` などでパラメタと実際の受け渡し時の変数名がずれてしまってちゃんと受け取れてない。
  対処方法を要確認。
  - おそらく[このバグ](https://github.com/OpenAPITools/openapi-generator/issues/4679)関連？
  - 他にも色々バグがありそうなので、注意
  - `collectRequestParams` 内において
    - `schema = request.openapi.schema.requestBody.content['application/json']` がそもそもおかしい。`schema` の値がほしいなら更に `['schema']` が必要なはず
      - 暫定対処：`schema = request.openapi.schema.requestBody.content['application/json']['schema']`
    - `requestParams[schema.$ref.substr(schema.$ref.lastIndexOf('.'))]` では正しい名前は取得できないはず。URL を想定してる？
      - 暫定対処：`requestParams[schema.$ref.substr(schema.$ref.lastIndexOf('/') + 1).toLowerCase()] = request.body;`

```
docker network create nodejs-net

docker run \
  --rm \
  -it \
  --name nodejs-sv \
  -p 13000:3000 \
  -e "NODE_ENV=production" \
  -u "node" \
  -v $(pwd)/nodejs:/home/node/app \
  -w "/home/node/app" \
  --network nodejs-net \
  node \
    npm start
```

## DB

- mongo

https://mongodb.github.io/node-mongodb-native/3.6/quick-start/quick-start/

```
npm install mongodb
```

`package.json`を上書き不可にしないと自動生成時に削除される。
