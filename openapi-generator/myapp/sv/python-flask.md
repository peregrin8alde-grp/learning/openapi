# python-flask

https://github.com/OpenAPITools/openapi-generator/blob/master/docs/generators/python-flask.md

```
java -jar ../../../../lib/openapi-generator-cli.jar \
    generate \
    -i ../doc/openapi.json \
    -g python-flask \
    -o ./python-flask \
    --ignore-file-override ./python-flask/openapi-generator-ignore
```

```
docker run \
  --rm \
  -it \
  --name python-flask-sv \
  -v $(pwd)/python-flask:/usr/src/app \
  -w /usr/src/app \
  python:3-alpine \
    pip3 install -r requirements.txt -t /usr/src/app/lib

docker run \
  --rm \
  -it \
  --name python-flask-sv \
  -p 8080 \
  -v $(pwd)/python-flask:/usr/src/app \
  -w /usr/src/app \
  --network kong-net \
  -e PYTHONPATH=/usr/src/app/lib \
  python:3-alpine \
    python3 -m openapi_server
```

```
docker build -t openapi_server ./python-flask

docker run \
  --rm \
  -d \
  --name python-flask-sv \
  -p 8080:8080 \
  openapi_server
```

http://localhost:8080/myapp/api/v1/ui/

ログの出力は以下参照

https://docs.python.org/ja/3/howto/logging.html

CORS は以下を利用予定（軽く試したところ上手く行かなかったが、本番では使わないため保留）

- https://flask-cors.readthedocs.io/en/latest/
- https://connexion.readthedocs.io/en/latest/cookbook.html#cors-support

## トラブルシューティング

`connexion`内の`snake_and_shadow`が以下のようになっており、`id`というパラメタ名が利用できない？
（`/usr/src/app/lib/connexion/decorators/parameter.py`）

```
def snake_and_shadow(name):
    """
    Converts the given name into Pythonic form. Firstly it converts CamelCase names to snake_case. Secondly it looks to
    see if the name matches a known built-in and if it does it appends an underscore to the name.
    :param name: The parameter name
    :type name: str
    :return:
    """
    snake = inflection.underscore(name)
    if snake in builtins.__dict__.keys():
        return "{}_".format(snake)
    return snake
```

```
dict_keys([
    '__name__', '__doc__', '__package__', '__loader__', '__spec__', '__build_class__', '__import__',
    'abs', 'all', 'any', 'ascii',
    'bin', 'breakpoint',
    'callable', 'chr', 'compile',
    'delattr', 'dir', 'divmod',
    'eval', 'exec',
    'format',
    'getattr', 'globals',
    'hasattr', 'hash', 'hex',
    'id', 'input', 'isinstance', 'issubclass', 'iter',
    'len', 'locals',
    'max', 'min',
    'next',
    'oct', 'ord',
    'pow', 'print',
    'repr', 'round',
    'setattr', 'sorted', 'sum',
    'vars',
    'None', 'Ellipsis', 'NotImplemented', 'False', 'True',
    'bool', 'memoryview', 'bytearray', 'bytes', 'classmethod',
    'complex', 'dict', 'enumerate', 'filter', 'float', 'frozenset',
    'property', 'int', 'list', 'map', 'object', 'range',
    'reversed', 'set', 'slice', 'staticmethod', 'str', 'super',
    'tuple', 'type', 'zip',
    '__debug__', 'BaseException', 'Exception', 'TypeError', 'StopAsyncIteration', 'StopIteration',
    'GeneratorExit', 'SystemExit', 'KeyboardInterrupt', 'ImportError',
    'ModuleNotFoundError', 'OSError', 'EnvironmentError', 'IOError',
    'EOFError', 'RuntimeError', 'RecursionError', 'NotImplementedError',
    'NameError', 'UnboundLocalError', 'AttributeError', 'SyntaxError',
    'IndentationError', 'TabError', 'LookupError', 'IndexError', 'KeyError',
    'ValueError', 'UnicodeError', 'UnicodeEncodeError', 'UnicodeDecodeError',
    'UnicodeTranslateError', 'AssertionError', 'ArithmeticError', 'FloatingPointError',
    'OverflowError', 'ZeroDivisionError', 'SystemError', 'ReferenceError', 'MemoryError',
    'BufferError', 'Warning', 'UserWarning', 'DeprecationWarning', 'PendingDeprecationWarning',
    'SyntaxWarning', 'RuntimeWarning', 'FutureWarning', 'ImportWarning', 'UnicodeWarning',
    'BytesWarning', 'ResourceWarning', 'ConnectionError', 'BlockingIOError', 'BrokenPipeError',
    'ChildProcessError', 'ConnectionAbortedError', 'ConnectionRefusedError', 'ConnectionResetError',
    'FileExistsError', 'FileNotFoundError', 'IsADirectoryError', 'NotADirectoryError', 'InterruptedError',
    'PermissionError', 'ProcessLookupError', 'TimeoutError',
    'open', 'quit', 'exit', 'copyright', 'credits', 'license', 'help'
    ])
```

`OpenAPI 3.x.x` では `requestBody` が名前を持たないが、connexion では名前が一致するかどうか
見てしまうため、スキーマ上で `x-body-name` を設定しないといけない。
（生成されるソースには影響与えない）

https://connexion.readthedocs.io/en/latest/request.html

```
In the OpenAPI 3.x.x spec, the requestBody does not have a name.
By default it will be passed in as ‘body’.
You can optionally provide the x-body-name parameter in your requestBody schema
to override the name of the parameter that will be passed to your handler function.
```

## DB

- mongo

https://pymongo.readthedocs.io/en/stable/#

```
export PYTHONPATH=./lib
pip install pymongo -t ${PYTHONPATH}
```
