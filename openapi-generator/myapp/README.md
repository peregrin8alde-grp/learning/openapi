# myapp

## ドキュメント

API の仕様や関数の各言語における使い方など。

```
java -jar ../../../lib/openapi-generator-cli.jar \
    generate \
    -i ./doc/openapi.json \
    -g html2 \
    -o ./doc/autogen/myapp
```

- `Item` 自体は `uid` 必須にしつつ、POST などのボディでは省略可能にするには？
  - typescript で NULL 許可の扱いが面倒なため
  - openapi としてはリクエスト時に必須のものかどうかの指定でしかないように見える。
    言語依存は避けたいため、こちらの考え方に合わせることにする。

## サーバ

- `uid` は連番だと攻撃しやすくなりそうなため、乱数とする。

### 動作確認

`doc/autogen/myapp` 参照

```
PORT=8080

curl -X GET "http://localhost:${PORT}/myapp/api/v1/todo" \
  -H "Content-Type: application/json" \
  -H "accept: application/json"

curl -X GET "http://localhost:${PORT}/myapp/api/v1/todo/1" \
  -H "Content-Type: application/json" \
  -H "accept: application/json"

curl -X POST "http://localhost:${PORT}/myapp/api/v1/todo" \
  -H "Content-Type: application/json" \
  -H "accept: application/json" \
  -d '{
    "task": "todo it",
    "isDone": false
  }'

curl -X PUT "http://localhost:${PORT}//myapp/api/v1/todo/1" \
  -H "Content-Type: application/json" \
  -H "accept: application/json" \
  -d '{
    "uid": "1",
    "task": "todo it",
    "isDone": true
  }'
```

## クライアント
