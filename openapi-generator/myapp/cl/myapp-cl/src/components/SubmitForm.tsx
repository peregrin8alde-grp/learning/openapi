import React from "react";

type Props = {
  onSubmit(event: React.FormEvent<HTMLFormElement>): void;
};

const SubmitForm: React.FC<Props> = (props) => {
  return (
    <form onSubmit={props.onSubmit}>
      <label>
        <input name="task" type="text" />
      </label>
      <input type="submit" value="Add" />
    </form>
  );
};

export default SubmitForm;
