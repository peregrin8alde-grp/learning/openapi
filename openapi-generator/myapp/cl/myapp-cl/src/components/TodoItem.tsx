import React from "react";
import * as MyappApi from "../autogen/myapp-api";

type Props = {
  item: MyappApi.Item;
  onChange(event: React.ChangeEvent<HTMLInputElement>): void;
  onSubmit(event: React.FormEvent<HTMLFormElement>): void;
  onClick(event: React.MouseEvent<HTMLButtonElement>): void;
};

type State = {
  task?: string;
};

class TodoItem extends React.Component<Props, State> {
  state: State = {
    task: this.props.item.task,
  };

  onChangeTask = (e: React.FormEvent<HTMLInputElement>): void => {
    console.log("onChange");
    console.log(e.target);
    console.log(e.currentTarget);
    this.setState({ task: e.currentTarget.value });
  };

  render() {
    return (
      <form onSubmit={this.props.onSubmit}>
        <label>{this.props.item.uid}:</label>
        <input
          name="isDone"
          type="checkbox"
          checked={this.props.item.isDone}
          onChange={this.props.onChange}
        />
        <input
          name="task"
          type="text"
          value={this.state.task}
          onChange={this.onChangeTask}
        />
        <button type="submit">Put</button>
        <button type="button" onClick={this.props.onClick}>
          Delete
        </button>
      </form>
    );
  }
}

export default TodoItem;
