import * as React from "react";
import * as MyappApi from "../autogen/myapp-api";

import SubmitForm from "./SubmitForm";
import TodoItem from "./TodoItem";

export interface ProcessEnv {
  [key: string]: string | undefined;
}

const server = process.env.REACT_APP_SERVER ? process.env.REACT_APP_SERVER : "";
const path = process.env.REACT_APP_PATH ? process.env.REACT_APP_PATH : "/";

const conf = new MyappApi.Configuration({
  basePath: server + path,
});
const api = new MyappApi.DefaultApi(conf);

type Props = {};

type State = {
  items: MyappApi.Item[];
  isLoaded: boolean;
  error: any; // errorの正しい型は？
};

class TodoList extends React.Component<Props, State> {
  state: State = {
    error: null,
    isLoaded: false,
    items: [],
  };

  componentDidMount() {
    api.todoGet().then(
      (result) => {
        console.log(result);
        this.setState({
          isLoaded: true,
          items: result,
        });
      },
      // 補足：コンポーネント内のバグによる例外を隠蔽しないためにも
      // catch()ブロックの代わりにここでエラーハンドリングすることが重要です
      (error) => {
        this.setState({
          error,
        });
        console.error(error);
      }
    );
  }

  handleAdd(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();

    const target = event.target as typeof event.target & {
      task: { value: string };
    };
    const task = target.task.value;

    api
      .todoPost({
        item: {
          task: task,
          isDone: false,
        },
      })
      .then(
        (result) => {
          console.log(result);

          const items = this.state.items.slice();
          this.setState({ items: items.concat([result]) });
        },
        // 補足：コンポーネント内のバグによる例外を隠蔽しないためにも
        // catch()ブロックの代わりにここでエラーハンドリングすることが重要です
        (error) => {
          this.setState({
            error,
          });
          console.error(error);
        }
      );

    target.task.value = "";
  }

  handlePut(uid: string, event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    console.log(event.target);
    console.log(event.currentTarget);

    const target = event.target as typeof event.target & {
      task: { value: string };
      isDone: { checked: boolean };
    };
    const task = target.task.value;
    const isDone = target.isDone.checked;

    api
      .todoUidPut({
        uid: uid,
        item: {
          task: task,
          isDone: isDone,
        },
      })
      .then(
        (result) => {
          console.log(result);

          const UpdateItems = (arr: MyappApi.Item[], newItem: MyappApi.Item) =>
            arr.map((item) => (item.uid === newItem.uid ? newItem : item));
          this.setState({ items: UpdateItems(this.state.items, result) });
        },
        // 補足：コンポーネント内のバグによる例外を隠蔽しないためにも
        // catch()ブロックの代わりにここでエラーハンドリングすることが重要です
        (error) => {
          this.setState({
            error,
          });
          console.error(error);
        }
      );
  }

  handlePatch(uid: string, event: React.ChangeEvent<HTMLInputElement>) {
    event.preventDefault();
    console.log(event);
    console.log(event.target);

    const isDone = event.target.checked;

    api
      .todoUidPatch({
        uid: uid,
        item: {
          isDone: isDone,
        },
      })
      .then(
        (result) => {
          console.log(result);

          const UpdateItems = (arr: MyappApi.Item[], newItem: MyappApi.Item) =>
            arr.map((item) => (item.uid === newItem.uid ? newItem : item));
          this.setState({ items: UpdateItems(this.state.items, result) });
        },
        // 補足：コンポーネント内のバグによる例外を隠蔽しないためにも
        // catch()ブロックの代わりにここでエラーハンドリングすることが重要です
        (error) => {
          this.setState({
            error,
          });
          console.error(error);
        }
      );
  }

  handleDelete(uid: string) {
    api
      .todoUidDelete({
        uid: uid,
      })
      .then(
        (result) => {
          console.log(result);

          const items = this.state.items.slice();
          this.setState({ items: items.filter((item) => item.uid !== uid) });
        },
        // 補足：コンポーネント内のバグによる例外を隠蔽しないためにも
        // catch()ブロックの代わりにここでエラーハンドリングすることが重要です
        (error) => {
          this.setState({
            error,
          });
          console.error(error);
        }
      );
  }

  render() {
    const { error, isLoaded, items } = this.state;

    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      const listItems = items.map((item) => (
        <li key={item.uid}>
          <TodoItem
            item={item}
            onChange={(e) => this.handlePatch(item.uid ? item.uid : "", e)} // undefined以外の場合だけ値を入れる方法は？
            onSubmit={(e) => this.handlePut(item.uid ? item.uid : "", e)} // undefined以外の場合だけ値を入れる方法は？
            onClick={(e) => this.handleDelete(item.uid ? item.uid : "")} // undefined以外の場合だけ値を入れる方法は？
          ></TodoItem>
        </li>
      ));
      return (
        <div>
          <ul>{listItems}</ul>
          <SubmitForm onSubmit={(e) => this.handleAdd(e)} />
        </div>
      );
    }
  }
}

export default TodoList;
