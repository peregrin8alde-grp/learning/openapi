import React from "react";
import "./App.css";

import TodoList from "./components/TodoList";

type Props = {};

const App: React.FC<Props> = (props) => {
  return (
    <div className="App">
      <TodoList />
    </div>
  );
};

export default App;
