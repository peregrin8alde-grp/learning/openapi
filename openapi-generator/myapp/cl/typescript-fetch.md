# typescript-fetch

参考：

- https://ja.reactjs.org/docs/static-type-checking.html#typescript
- https://github.com/Microsoft/TypeScript-React-Starter#typescript-react-starter
- https://create-react-app.dev/docs/adding-typescript

- サーバ設定は [環境変数で設定](https://create-react-app.dev/docs/adding-custom-environment-variables)で行う。
  - Node.js 用の設定方法では利用できないため注意。
  - ビルド時にハードコーディングされるため、公開可能な情報以外設定しないこと。

```
docker run \
  --rm \
  -it \
  -u "node" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app" \
  node \
    npx create-react-app myapp-cl --template typescript

### git などで clone して node_modules がない状態から始める場合
docker run \
  --rm \
  -it \
  -u "node" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app/myapp-cl" \
  node \
    npm install

### GlobalFetch が使えなくなったバージョン用に typescriptThreePlus指定
java -jar ../../../../lib/openapi-generator-cli.jar \
    generate \
    -i ../doc/openapi.json \
    -g typescript-fetch \
    -p typescriptThreePlus='true' \
    -o ./myapp-cl/src/autogen/myapp-api

docker run \
  --rm \
  -it \
  -p 3000:3000 \
  -u "node" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app/myapp-cl" \
  -e REACT_APP_SERVER="http://localhost:8000" \
  -e REACT_APP_PATH="/python-flask-sv/myapp/api/v1" \
  node \
    yarn start

docker run \
  --rm \
  -it \
  -u "node" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app/myapp-cl" \
  -e REACT_APP_SERVER="http://localhost:8000" \
  -e REACT_APP_PATH="/python-flask-sv/myapp/api/v1" \
  node \
    yarn run build

# Webサーバ単体実行
docker run \
  -d \
  --name myapp-cl \
  -p 8080:80 \
  -v $(pwd)/myapp-cl/build:/usr/share/nginx/html:ro \
  --network kong-net \
  nginx:alpine
```

http://localhost:8080

## トラブルシューティング

typescript のバージョンによってはバグで以下のエラー？

```
./src/autogen/myapp-api/index.ts
  Line 0:  Parsing error: Cannot read property 'name' of undefined
```

```
"typescript": "~3.7.2"
↓
"typescript": "3.8.3"
```

肝心のエラー原因モジュールが入れ替わってないので以下も追加し、`node_module` および `lock` 系ファイルを削除し全インストール。

```
"resolutions": {
"@typescript-eslint/eslint-plugin": "2.23.0",
"@typescript-eslint/parser": "2.23.0",
"@typescript-eslint/typescript-estree": "2.23.0"
},
```

```
docker run \
  --rm \
  -it \
  -u "node" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app/myapp-cl" \
  node \
    npm install
```
