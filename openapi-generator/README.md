# openapi-generator

https://github.com/OpenAPITools/openapi-generator

https://www.slideshare.net/techblogyahoo/swagger-openapi-specification-30-api

## install jar

```
mkdir lib

curl -o ../../lib/openapi-generator-cli.jar \
  https://repo1.maven.org/maven2/org/openapitools/openapi-generator-cli/4.3.1/openapi-generator-cli-4.3.1.jar

java -jar ../../lib/openapi-generator-cli.jar help
```

## docker

- https://hub.docker.com/r/openapitools/openapi-generator-cli/

```
docker run \
  --rm \
  -v ${PWD}:/local \
  openapitools/openapi-generator-cli \
    generate \
    -i https://raw.githubusercontent.com/openapitools/openapi-generator/master/modules/openapi-generator/src/test/resources/2_0/petstore.yaml \
    -g go \
    -o /local/out/go

docker run \
  --rm \
  -v ${PWD}:/local \
  openapitools/openapi-generator-cli \
    generate \
    -i https://raw.githubusercontent.com/openapitools/openapi-generator/master/modules/openapi-generator/src/test/resources/2_0/petstore.yaml \
    -g javascript \
    -o /local/out/javascript
```

- https://hub.docker.com/r/openapitools/openapi-generator-online/

```
docker run \
  -d \
  --name openapi-gen \
  -e GENERATOR_HOST=http://12.34.56.78 \
  -p 80:8080 \
  openapitools/openapi-generator-online
```
